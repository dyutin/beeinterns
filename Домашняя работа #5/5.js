/* 5. Необходимо проверить на тип переменную a. Посмотреть, что выводит код и написать объяснение в комментарий
 */

/* Код выводит Hello world */

let a = "Hello world";

if (typeof(a) === "string") { /* Добавляем проверку типов с помощью typeof(), если в переменной 'a' значение типа строка, выводим Hello world */
	console.log(a);
}
