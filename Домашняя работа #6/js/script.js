/* 1 задание */


function toUpperCase() {
    let headings = document.querySelectorAll(".item-heading");
    for (let i = 0; i < headings.length; i++) {
        headings[i].innerHTML = headings[i].innerHTML.toUpperCase();
    }
}


/* 2 задание */


function compressDescriptions() {
    let descriptions = document.querySelectorAll(".item-description");
    for (let i = 0; i < descriptions.length; i++) {
        console.log(descriptions[i].innerHTML.length)
        if (descriptions[i].innerHTML.length >= 20) {
            descriptions[i].innerHTML = descriptions[i].innerHTML.slice(0, 20) + "...";
        }
    }
}

toUpperCase();
compressDescriptions();



/* слайдер */


let width = 261;
let count = 1;

document.querySelectorAll('.container-quest').forEach(function (carousel) {
    let list = carousel.querySelector('.items');
    let listElems = carousel.querySelectorAll('.item');
    let position = 0;
    carousel.querySelector('.back').onclick = function () {
        position += width * count;
        position = Math.min(position, 0)
        list.style.marginLeft = position + 'px';
    };
    carousel.querySelector('.next').onclick = function () {
        position -= width * count;
        position = Math.max(position, -width * (listElems.length - count));
        list.style.marginLeft = position + 'px';
    };
});